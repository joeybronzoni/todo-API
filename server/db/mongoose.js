const mongoose = require('mongoose');

// import environmental variables from our variables.env file
require('dotenv').config({ path: 'variables.env' });

let env  = process.env.NODE_ENV || 'development' || 'mlab';

mongoose.Promise = global.Promise;

if (env === 'mlab') {
mongoose.connect(process.env.REMOTE_DATABASE);
} else if (env === 'development') {
mongoose.connect(process.env.DATABASE);
} else {
mongoose.connect(process.env.MONGODB_URI);
}

module.exports = { mongoose };